# Introduction to Matrix
### Federated, Feature-Rich Chat
 * Barry Peddycord, former LUG officer, Class of '11, '13
 * https://isharacomix.org

## What is Federation?
Federation is a step between peer-to-peer and walled-garden services.

In a Federated ecosystem, a user delegates tasks to a homeserver. That
homeserver is responsible for sharing outbound messages to users on other
servers, and at the same time collecting messages for its own users. This allows
distributed, resilient communication with no single point of failure.

![](matrix-diagram.png)

## What is "Feature-Rich" Chat?

 * Slack, Discord are examples of Feature-Rich
 * IRC is an example of Feature-Poor
 * XMPP is kind of a gray area

Feature rich chat is most useful in *shared communities* where *moderation* is
needed.

![](riot-screenshot.png)



## What are the benefits of Federation?

 * You can access chats and interact with users on any Homeserver in the ecosystem without making multiple accounts
 * If there is a malicious Homeserver in the ecosystem, your Homeserver can refuse to federate
 * If you want full control over your security, you can make a single-user Homeserver


## How do you join Matrix?
In order to join the *Matrix Ecosystem*, you must use a *Client* to create an
account and log into a *Homeserver*. The Homeserver stores all of the chat logs
for the rooms you are in, and syncs them with the Homeservers of other users
in the room

The flagship Homeserver is at `matrix.org`, and the flagship web client is at
`riot.im` (there are also dedicated desktop clients, and a variety of plugins
for other clients such as Pidgin, Irssi, etc).

![](riot-login.png)


## Finding people and rooms

Your full username in Matrix looks something like `@user:homeserver`.

 * I am `@isharacomix:matrix.org`

A room name looks something like `#room:homeserver`

The homeserver portion of the room's name is simply a formality that indicates
where the room originated. Even if the original homeserver is gone, the room
still exists and its users can still communicate and invite new users. "Private
Chats" are just rooms with secret IDs that can't be joined without an
invitation.


## Bridging
In order to increase adoption, Matrix has coordinated with Freenode staff to
provide a bridge for its users.

 * Try joining `#freenode_#ncsulug:matrix.org`

Matrix users will appear with an `[m]` in their nickname unless they auth to
services through Nickserv, at which point you won't be able to tell them apart
from ordinary IRC client users (until the bridge disconnects and nickserv
strips away their nickname)

 * https://gist.github.com/fstab/ce805d3001600ac147b79d413668770d


## Why use Matrix instead of Slack/Discord?

 * Open source, open protocols, bring/build your own client

## Why use Matrix instead of IRC?
 
 * Single account grants access to all servers
 * No need to run 3rd-party bouncers or always-on clients (Homeserver is the bouncer)
 * Better moderation of channels
 * Support for Communities (groups of channels)

## Anything you shouldn't use Matrix for?

Secure 1:1 communication. While there is end-to-end encryption, the risk of
compromised clients and homeservers still exist rendering this unsuitable.

## NCSULUG Matrix Community

Visit [+ncsulug:matrix.org](https://matrix.to/#/+ncsulug:matrix.org) to join the
NCSULUG Matrix Community. This provides a link to the IRC channel, but will
also let you see who else is using Matrix and will allow you to create new
channels when Matrix eventually takes over the world!

To join a community (or a channel) via a hyperlink copy the url into a window in
which you are already using Riot.
